import 'package:flutter_test/flutter_test.dart';
import 'package:numbers_trivia/core/network/network_info.dart';

void main() {
  NetworkInfoImpl networkInfo;

  setUp(() {
    networkInfo = NetworkInfoImpl();
  });

  group('isConnected', () {
    test(
      'should return true',
      () async {
        // arrange
        final tHasConnectionFuture = Future.value(true);

        // act
        // NOTICE: We're NOT awaiting the result
        final result = networkInfo.isConnected;
        // Utilizing Dart's default referential equality.
        // Only references to the same object are equal.
        expect(tHasConnectionFuture, tHasConnectionFuture);
      },
    );
  });
}
