import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  const Failure();
}

// General failures
class ServerFailure extends Failure {
  const ServerFailure();

  @override
  List<Object> get props => [];
}

class CacheFailure extends Failure {
  const CacheFailure();

  @override
  List<Object> get props => [];
}