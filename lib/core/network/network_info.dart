import 'dart:math';

abstract class NetworkInfo {
  Future<bool> get isConnected;
}

class NetworkInfoImpl implements NetworkInfo {
  NetworkInfoImpl();

  // @override
  // Future<bool> get isConnected => Future.value(Random().nextBool());
  @override
  Future<bool> get isConnected => Future.value(true);
}
